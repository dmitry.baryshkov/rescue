#!/bin/sh

QCOM_LINUX_FIRMWARE='http://releases.linaro.org/96boards/dragonboard820c/qualcomm/firmware/linux-board-support-package-r01700.1.zip'
QCOM_LINUX_FIRMWARE_MD5='587138c5e677342db9a88d5c8747ec6c'
QCOM_LINUX_FIRMWARE_LICENSE_MD5='4d087ee0965cb059f1b2f9429e166f64'

LK_EMMC_REF='release/LA.HB.1.3.2-19600-8x96.0'
#LK_RESCUE_REF='release/LA.HB.1.3.2-19600-8x96.0+rescue'

BASE_NAME="dragonboard-820c"
USER_NAME="Dragonboard 820c"
PUBLISH_NAME="dragonboard820c"

. ./builders.sh

download_firmware

extract_firmware \
    "*/LICENSE" \
    "*/bootloaders-linux/*" \
    "*/cdt-linux/*" \
    "*/loaders/*"

rm -f ${DRAGONBOARD}-bootloaders-linux/gpt*
rm -f ${DRAGONBOARD}-bootloaders-linux/xbl.sd.elf
rm -f ${DRAGONBOARD}-bootloaders-linux/pmic.sd.elf
rm -f ${DRAGONBOARD}-bootloaders-linux/prog_ufs_firehose_8996_lite.elf
rm -f ${DRAGONBOARD}-bootloaders-linux/sec.dat
rm -f ${DRAGONBOARD}-bootloaders-linux/BTFM.bin

check_license ${DRAGONBOARD}-bootloaders-linux/LICENSE

# Empty/zero boot image file to clear boot partition
dd if=/dev/zero of=boot-erase.img bs=1024 count=1024

> list.in
> parts.in

get_build_artifact "${LK_API}" "${LK_EMMC_REF}" build emmc.zip

unzip -o -j -d emmc emmc.zip

echo '* Little Kernel (LK) source code:' >> parts.in

echo '** "eMMC Linux boot":'$(get_commit_url "${LK_API}" $(cat emmc/commit) | sed -e 's/commit/tree/g') >> parts.in

echo '* Partition table:' >> parts.in

get_boot_files linux Linux
get_boot_files aosp  AOSP

rm -f ${DRAGONBOARD}-linux/gpt_empty*
rm -f ${DRAGONBOARD}-aosp/gpt_empty*

do_rescue linux Linux ufs "onboard UFS" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-linux/* \
	emmc/emmc_appsboot.mbn \
        boot-erase.img \
        LICENSE
do_rescue aosp  AOSP  ufs "onboard UFS" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-aosp/* \
	emmc/emmc_appsboot.mbn \
        boot-erase.img \
        LICENSE

cd out2
md5sum *.zip > MD5SUMS.txt
cd ..

do_header
do_publish
