#!/bin/sh

QCOM_LINUX_FIRMWARE='https://releases.linaro.org/96boards/rb1/qualcomm/firmware/RB1_firmware_20231124-v4.zip'
QCOM_LINUX_FIRMWARE_MD5='ddcb7d78e2c30d8fff0f605410ca7738'
QCOM_LINUX_FIRMWARE_LICENSE_MD5='cbbe399f2c983ad51768f4561587f000'

BASE_NAME="rb1"
USER_NAME="Qualcomm Robotics RB1"
PUBLISH_NAME="qrb2210-rb1"

. ./builders.sh

download_firmware

extract_firmware \
    "*/LICENSE.qcom.txt" \
    "*/02-firehose_prog/prog_firehose_ddr.elf" \
    "*/03-devcfg/devcfg.mbn" \
    "*/04-dspso/dspso.bin" \
    "*/05-hyp/hyp.mbn" \
    "*/06-non-hlos/NON-HLOS.bin" \
    "*/07-storsec/storsec.mbn" \
    "*/08-tz/tz.mbn" \
    "*/09-xbl/xbl.elf" \
    "*/09-xbl/xbl_feature_config.elf" \
    "*/19-km4/km4.mbn" \
    "*/22-qupv3fw/qupv3fw.elf" \
    "*/24-multi_image/multi_image.mbn" \
    "*/27-uefi_sec/uefi_sec.mbn" \
    "*/28-imagefv/imagefv.elf" \
    "*/29-abl/abl.elf" \
    "*/33-rpm/rpm.mbn" \
    "*/34-featenabler/featenabler.mbn" \
    "*/35-BTFM/BTFM.bin" \

check_license ${DRAGONBOARD}-bootloaders-linux/LICENSE.qcom.txt

> list.in
> parts.in

echo '* Partition table:' >> parts.in

get_boot_files linux Linux
get_boot_files aosp  AOSP

# TODO: add support for manually built ABL

rm -f ${DRAGONBOARD}-linux/gpt_empty*
rm -f ${DRAGONBOARD}-linux/zeros*
rm -f ${DRAGONBOARD}-aosp/gpt_empty*
rm -f ${DRAGONBOARD}-aosp/zeros*

do_rescue linux Linux emmc "onboard eMMC" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-linux/* \
        boot-erase.img \
        LICENSE
do_rescue aosp  AOSP  emmc "onboard eMMC" \
        ${DRAGONBOARD}-bootloaders-linux/* \
        ${DRAGONBOARD}-aosp/* \
        boot-erase.img \
        LICENSE

echo >> list.in
echo "**NOTE:** Don't forget to pass @--storage emmc@ to QDL if you need to use it." >> list.in

cd out2
md5sum *.zip > MD5SUMS.txt
cd ..

do_header
do_publish
